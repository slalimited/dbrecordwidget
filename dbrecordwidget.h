#ifndef DBRECORDWIDGET_H
#define DBRECORDWIDGET_H

#include "dbrecordwidget_global.h"
#include <QWidget>
#include <QVector>
#include <QPushButton>
#include <QMap>
#include <QMetaEnum>
#include <QDebug>

namespace Ui {
class DBRecordWidget;
}

namespace operationButtons {
Q_NAMESPACE
enum buttons {insert=0, update, remove, import_, export_, report, refresh,
             save, print, unusedCodes};
Q_ENUM_NS(buttons)
}



class DBRECORDWIDGETSHARED_EXPORT DBRecordWidget : public QWidget
{
    Q_OBJECT
public:
    DBRecordWidget();
    Ui::DBRecordWidget *ui;
    DBRecordWidget(QWidget *parent);
signals:
    //clicked() signal for different buttons
    void insertClicked();
    void updateClicked();
    void removeClicked();
    void importClicked();
    void exportClicked();
    void reportClicked();
    void refreshClicked();
    void saveClicked();
    void printClicked();
    void unusedCodesClicked();
public slots:
    //sets user font size for widget
    void setOwnFontSize(int fontSize);
    //sets user buttons icon size
    void setIconSize(int iconWidth, int iconHeight);
    //sets visible widget buttons
    void setVisibleOperationButtons(
            QVector<operationButtons::buttons> visibleButtons);
    void setUserCaption(operationButtons::buttons type, QString caption);
private slots:
    //analyzes clicked button type
    void buttonClicked();
    //sets names for buttons (in Russian)
    void fillButtonsMap();
private:
    //vector for visible buttons
    QVector<QPushButton*> visibleButtons;
    //vector for button names
    QMap<operationButtons::buttons, QString> buttonNames;

};

#endif // DBRECORDWIDGET_H
