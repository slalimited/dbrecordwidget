#include "dbrecordwidget.h"
#include "ui_dbrecordwidget.h"

DBRecordWidget::DBRecordWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DBRecordWidget)
{
    ui->setupUi(this);
    fillButtonsMap();
}

void DBRecordWidget::fillButtonsMap()
{
    qDebug() << "DBRecordWidget::fillButtonsMap()";
    buttonNames.clear();
    buttonNames.insert(operationButtons::insert, tr("Добавить"));
    buttonNames.insert(operationButtons::update, tr("Изменить"));
    buttonNames.insert(operationButtons::remove, tr("Удалить"));
    buttonNames.insert(operationButtons::import_, tr("Импорт"));
    buttonNames.insert(operationButtons::export_, tr("Экспорт"));
    buttonNames.insert(operationButtons::report, tr("Отчет"));
    buttonNames.insert(operationButtons::refresh, tr("Обновить"));
    buttonNames.insert(operationButtons::save, tr("Сохранить"));
    buttonNames.insert(operationButtons::print, tr("Печать"));
    buttonNames.insert(operationButtons::unusedCodes,
                       tr("Неверифицированные\nкоды маркировки"));
}

void DBRecordWidget::setUserCaption(operationButtons::buttons type,
                                    QString caption)
{
    for(int i = 0; i < visibleButtons.size(); ++i)
        if (visibleButtons[i]->text() == buttonNames[type]) {
            visibleButtons[i]->setText(caption);
            buttonNames.insert(type, caption);
            break;
        }
}

void DBRecordWidget::setOwnFontSize(int fontSize)
{
    QFont font = this->font();
    font.setPointSize(fontSize);
    this->setFont(font);
    this->resize(this->sizeHint());
}

void DBRecordWidget::setIconSize(int iconWidth, int iconHeight)
{
    for(int i = 0; i < visibleButtons.size(); ++i)
        visibleButtons[i]->setIconSize(QSize(iconWidth, iconHeight));
    this->resize(this->sizeHint());
}

void DBRecordWidget::setVisibleOperationButtons(
        QVector<operationButtons::buttons> visibleButtons)
{
    QMetaEnum enumButtons = QMetaEnum::fromType<operationButtons::buttons>();
    for(int i = 0; i < visibleButtons.size(); ++i) {
        QString buttonName(enumButtons.key(visibleButtons[i]));
        QPushButton *btn = new QPushButton;
        btn->setObjectName(buttonName);
        connect(btn, SIGNAL(clicked(bool)), this, SLOT(buttonClicked()));
        btn->setText(buttonNames.value(visibleButtons[i]));
        //pickes ico for the button named the same
        QString pixmapName = ":/img/"+buttonName+".png";
        btn->setIcon(QPixmap(pixmapName));
        ui->operationButtonsLayout->addWidget(btn);
        this->visibleButtons << btn;
    }
    ui->operationButtonsLayout->addStretch(1);
    this->resize(this->sizeHint());
}

void DBRecordWidget::buttonClicked()
{
    QString sender = QObject::sender()->objectName();
    if (sender == "insert")
        emit insertClicked();
    if (sender == "update")
        emit updateClicked();
    if (sender == "remove")
        emit removeClicked();
    if (sender == "import_")
        emit importClicked();
    if (sender == "export_")
        emit exportClicked();
    if (sender == "report")
        emit reportClicked();
    if (sender == "refresh")
        emit refreshClicked();
    if (sender == "save")
        emit saveClicked();
    if (sender == "print")
        emit printClicked();
    if (sender == "unusedCodes")
        emit unusedCodesClicked();
}

